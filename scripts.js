var todoList = [];
var text = "";
// var div = document.getElementsByTagName("div");
var input = document.getElementsByTagName("input");
var id = 0;


function domRefresh() {
  document.querySelector(".event-list").innerHTML = "";
  for (i = 0; i < todoList.length; i++) {
    var parent = document.getElementsByClassName("event-list");
    var listNode = document.createElement("li");
    var checkboxNode = document.createElement("input");
    var closeButtonNode = document.createElement("button");
    var paraTagNode = document.createElement("p");
    var eventTextNode = document.createTextNode(todoList[i][1]);
    paraTagNode.appendChild(eventTextNode);
    listNode.appendChild(checkboxNode);
    listNode.appendChild(paraTagNode);
    listNode.appendChild(closeButtonNode);
    parent[0].appendChild(listNode);
    listNode.setAttribute("id", todoList[i][0]);
    checkboxNode.setAttribute("type", "checkbox");
    checkboxNode.setAttribute("onclick", "checkEvent(this.parentElement)");
    closeButtonNode.setAttribute("onclick", "removeEvent(this.parentElement)");
    closeButtonNode.innerHTML = "X";

    if (todoList[i][2] == true) {
      paraTagNode.style.textDecoration = "line-through";
      paraTagNode.style.color = "#D3D3D3";
      checkboxNode.checked = true;
    } else {

      paraTagNode.style.textDecoration = "none";
      paraTagNode.style.color = "	#696969";
      checkboxNode.checked = false;


    }

  }
}


function addEvent(event) {
  if (event.keyCode == 13 || event.which == 13) {
    var todoObject = {
      id: id,
      text: text,
      isCompleted: false,
    };
    todoList.push([todoObject.id, todoObject.text, todoObject.isCompleted]);
    id += 1;
    text = "";
    input[0].value = "";
    domRefresh();

  } else {
    text += event.key;
  }
}

function removeEvent(item) {
  for (i = 0; i < todoList.length; i++) {
    if (todoList[i][0] == item.id) {
      todoList.splice(i, 1);
      break;
    }
  }
  domRefresh();
}

function checkEvent(item) {
  for (i = 0; i < todoList.length; i++) {
    if (todoList[i][2] == 0 && todoList[i][0] == item.id) {
      todoList[i][2] = true;
      break;
    } else if (todoList[i][2] == 1 && todoList[i][0] == item.id) {
      todoList[i][2] = false;
      break;
    }
  }
  domRefresh();
}
